package com.shanghe.moduletest;

import android.widget.Toast;

import com.uzmap.pkg.uzcore.UZWebView;
import com.uzmap.pkg.uzcore.uzmodule.UZModule;
import com.uzmap.pkg.uzcore.uzmodule.UZModuleContext;

import org.json.JSONException;
import org.json.JSONObject;

public class JsApi extends UZModule {
    public JsApi(UZWebView webView) {
        super(webView);
    }

    //语音合成
    public void jsmethod_test(final UZModuleContext moduleContext) {
        String msg = moduleContext.optString("text");
        Toast.makeText(context(), msg, Toast.LENGTH_SHORT).show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("msg", "测试内容");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        moduleContext.success(jsonObject, true);
    }
}
