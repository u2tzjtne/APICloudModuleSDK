package com.example.shbusqrcode.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.shbusqrcode.R;
import com.example.shbusqrcode.api.ServerApi;
import com.example.shbusqrcode.autosize.utils.AutoSizeUtils;
import com.example.shbusqrcode.util.NetworkUtil;
import com.example.shbusqrcode.util.QRCodeUtil;
import com.example.shbusqrcode.view.CircularImageView;
import com.example.shbusqrcode.view.dialog.KProgressHUD;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NativeBusQRCodeActivity extends Activity {
    private ImageView mQRCode;
    private CircularImageView mAvatar;
    private TextView mNickname;
    private String userId, deviceId, token, coordinate, recSign, errorCode, userPhoto, userName, serverUrl;
    private KProgressHUD dialog;
    private static final long TOTAL_TIME = 60000;
    private static final long ONECE_TIME = 30000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_native_bus_qrcode);
        mAvatar = findViewById(R.id.image_avatar);
        mQRCode = findViewById(R.id.image_qr_code);
        mNickname = findViewById(R.id.text_nickname);
        dialog = KProgressHUD.create(this).setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        setWindowMaxBrightness();//调整屏幕亮度
        Intent intent = getIntent();
        if (intent != null) {
            userId = intent.getStringExtra("userId");
            deviceId = intent.getStringExtra("deviceId");
            token = intent.getStringExtra("token");
            coordinate = intent.getStringExtra("coordinate");
            userPhoto = intent.getStringExtra("userPhoto");
            userName = intent.getStringExtra("userName");
            serverUrl = intent.getStringExtra("serverUrl");
            createQRCode();
            setUserInfo();
        }
    }

    //设置用户信息
    private void setUserInfo() {
        if (!TextUtils.isEmpty(userName)) mNickname.setText(userName);
        if (!TextUtils.isEmpty(userPhoto))
            Picasso.with(NativeBusQRCodeActivity.this).load(userPhoto).error(R.mipmap.icon_avatar).into(mAvatar);
    }

    //自动刷新
    private CountDownTimer countDownTimer = new CountDownTimer(TOTAL_TIME, ONECE_TIME) {
        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {
            createQRCode();
        }
    };

    //点击刷新
    public void refreshClick(View v) {
        countDownTimer.cancel();
        createQRCode();
    }

    //返回按钮
    public void backClick(View v) {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        countDownTimer.cancel();
        finish();
    }

    //请求数据并生成二维码
    private void createQRCode() {
        if (NetworkUtil.isNetworkAvailable(NativeBusQRCodeActivity.this)) {
            if (!dialog.isShowing()) dialog.show();
            try {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(serverUrl)
                        .client(new OkHttpClient())
                        .build();
                ServerApi serverApi = retrofit.create(ServerApi.class);
                String userAgent = new WebView(this).getSettings().getUserAgentString();
                Call<ResponseBody> call = serverApi.getQrCodeData(userAgent, token, deviceId, userId, coordinate, deviceId);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                        if (response.code() == 200) {
                            try {
                                String json = response.body().string();
                                if (json != null) {
                                    JSONObject jsonObject = new JSONObject(json);
                                    JSONObject body = jsonObject.getJSONObject("body");
                                    errorCode = body.getString("errorCode");
                                    recSign = body.getString("recSign");
                                    createQRAndDisplay();
                                } else {
                                    showErrorMessage(R.string.bus_error_message_qrcode, true);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                                showErrorMessage(R.string.bus_error_message_qrcode, true);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                showErrorMessage(R.string.bus_error_message_qrcode, true);
                            }
                        } else {
                            showErrorMessage(R.string.bus_error_message_qrcode, true);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                        showErrorMessage(R.string.bus_error_message_qrcode, true);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            createQRAndDisplay();
        }
    }

    //生成并显示二维码
    private void createQRAndDisplay() {
        if (!TextUtils.isEmpty(errorCode) && !TextUtils.isEmpty(recSign) && errorCode.equals("000000")) {
            if (!dialog.isShowing()) dialog.show();
            String str = createQRStr(recSign);
            final Bitmap bitmap = QRCodeUtil.createQRCode(str, AutoSizeUtils.dp2px(NativeBusQRCodeActivity.this, 220));
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (bitmap != null) {
                        mQRCode.setImageBitmap(bitmap);
                    } else {
                        showErrorMessage(R.string.bus_error_message_qrcode, false);
                    }
                    countDownTimer.cancel();
                    countDownTimer.start();
                }
            });
        }
        if (dialog.isShowing()) dialog.dismiss();
    }

    //生成二维码字符串
    private String createQRStr(String recSign) {
        if (!TextUtils.isEmpty(recSign)) {
            //当前时间
            String currentDate = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            //随机数
            int random = (int) (Math.random() * 10000 % 9 + 1);
            //当前时间乘以随机数
            long date = Long.parseLong(currentDate);
            StringBuilder dates = new StringBuilder(String.valueOf(date * random));
            //补足15位
            while (dates.length() < 15) {
                dates.insert(0, "0");
            }
            String str1 = random + "" + dates.toString() + recSign;
            StringBuilder lengths = new StringBuilder(String.valueOf(str1.length()));
            while (lengths.length() < 4) {
                lengths.insert(0, "0");
            }
            return ':' + lengths.toString() + random + "" + dates.toString() + recSign;
        }
        return null;
    }

    //设置当前窗口最高亮度
    private void setWindowMaxBrightness() {
        Window window = getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.screenBrightness = 1;
        window.setAttributes(lp);
    }

    //显示异常信息
    private void showErrorMessage(final int message, final boolean isReTry) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (dialog.isShowing())
                    dialog.dismiss();
                Toast.makeText(NativeBusQRCodeActivity.this, getResources().getString(message), Toast.LENGTH_SHORT).show();
                if (isReTry) {
                    countDownTimer.cancel();
                    countDownTimer.start();
                }
            }
        });
    }

    //返回键监听
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        countDownTimer.cancel();
        finish();
    }

    //    //获取用户信息并显示
//    private void getUserInfo() {
//        if (!dialog.isShowing()) dialog.show();
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(POST_URL)
//                .client(new OkHttpClient())
//                .build();
//        ServerApi serverApi = retrofit.create(ServerApi.class);
//        Call<ResponseBody> call = serverApi.getUserInfo(token, deviceId, userId);
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                isGetUserInfoComplete = true;
//                if (response.code() == 200) {
//                    try {
//                        String json = response.body().string();
//                        if (json != null) {
//                            JSONObject jsonObject = new JSONObject(json);
//                            JSONObject body = jsonObject.getJSONObject("body");
//                            userName = body.getString("userName");
//                            userPhoto = body.getString("userPhoto");
//                            if (!TextUtils.isEmpty(userName)) {
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        mNickname.setText(userName);
//                                    }
//                                });
//                            }
//                            if (!TextUtils.isEmpty(userPhoto)) {
//                                if (!userPhoto.startsWith("http"))
//                                    userPhoto = AVATAR_URL + userPhoto;
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        Picasso.with(NativeBusQRCodeActivity.this).load(userPhoto).error(R.mipmap.icon_avatar).into(mAvatar);
//                                    }
//                                });
//                            }
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    if (dialog.isShowing() && isGetQRInfoComplete) dialog.dismiss();
//                                }
//                            });
//                        } else {
//                            showErrorMessage(R.string.bus_error_message_userinfo, false);
//                        }
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                        showErrorMessage(R.string.bus_error_message_userinfo, false);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        showErrorMessage(R.string.bus_error_message_userinfo, false);
//                    }
//                } else {
//                    showErrorMessage(R.string.bus_error_message_userinfo, false);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
//                isGetUserInfoComplete = true;
//                showErrorMessage(R.string.bus_error_message_userinfo, false);
//            }
//        });
//    }

}
