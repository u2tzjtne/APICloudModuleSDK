package com.example.shbusqrcode.api;

import android.webkit.WebView;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * <pre>
 *     author : U2tzJTNE
 *     e-mail : u2tzjtne@gmail.com
 *     time   : 2018/10/09
 *     desc   :
 *     version: 1.0
 * </pre>
 */
public interface ServerApi {
    @FormUrlEncoded
    @POST("bus/produceTicket.do")
    Call<ResponseBody> getQrCodeData(@Header("User-Agent") String userAgent,@Header("token") String token, @Header("deviceId") String deviceID, @Field("userId") String userId, @Field("coordinate") String coordinate, @Field("mobileEquNum") String deviceId);


//    @FormUrlEncoded
//    @POST("user/getUserInfo.do")
//    Call<ResponseBody> getUserInfo(@Header("token") String token,@Header("deviceId") String deviceID,@Field("userId") String userId);
}
