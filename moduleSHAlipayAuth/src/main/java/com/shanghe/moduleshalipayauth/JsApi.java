package com.shanghe.moduleshalipayauth;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.alipay.sdk.app.AuthTask;
import com.uzmap.pkg.uzcore.UZWebView;
import com.uzmap.pkg.uzcore.uzmodule.UZModule;
import com.uzmap.pkg.uzcore.uzmodule.UZModuleContext;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class JsApi extends UZModule {

    private static final int SDK_AUTH_FLAG = 10001;

    public JsApi(UZWebView webView) {
        super(webView);
    }

    //支付宝账户授权登录
    public void jsmethod_SHAuthV2(final UZModuleContext moduleContext) {
        final String authInfo = moduleContext.optString("authInfo");
        if (!TextUtils.isEmpty(authInfo)) {
            @SuppressLint("HandlerLeak") final Handler mHandler = new Handler() {
                @SuppressWarnings("unused")
                public void handleMessage(Message msg) {
                    switch (msg.what) {
                        case SDK_AUTH_FLAG: {
                            Log.d("jsmethod_SHAuthV2", msg.obj.toString());
                            JSONObject jsonObject = new JSONObject();
                            Map<String, String> map = (Map<String, String>) msg.obj;
                            for (String key : map.keySet()) {
                                try {
                                    jsonObject.put(key, map.get(key));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            moduleContext.success(jsonObject, true);
                            break;
                        }
                    }
                }
            };
            Runnable authRunnable = new Runnable() {
                @Override
                public void run() {
                    // 构造AuthTask 对象
                    AuthTask authTask = new AuthTask(activity());
                    // 调用授权接口，获取授权结果
                    Map<String, String> result = authTask.authV2(authInfo, true);
                    Message msg = new Message();
                    msg.what = SDK_AUTH_FLAG;
                    msg.obj = result;
                    mHandler.sendMessage(msg);
                }
            };
            // 必须异步调用
            Thread authThread = new Thread(authRunnable);
            authThread.start();
        } else {
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("resultStatus", "9001");
                jsonObject.put("result", "授权失败");
                jsonObject.put("memo", "授权失败");
                moduleContext.success(jsonObject, true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
