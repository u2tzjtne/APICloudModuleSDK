package com.example.shbusqrcode.api;

import android.content.Intent;
import android.text.TextUtils;
import android.widget.Toast;

import com.example.shbusqrcode.R;
import com.example.shbusqrcode.activity.NativeBusQRCodeActivity;
import com.example.shbusqrcode.activity.NativeBusQRCodeActivity2;
import com.uzmap.pkg.uzcore.UZWebView;
import com.uzmap.pkg.uzcore.uzmodule.UZModule;
import com.uzmap.pkg.uzcore.uzmodule.UZModuleContext;

import org.greenrobot.eventbus.EventBus;

/**
 * <pre>
 *     author : U2tzJTNE
 *     e-mail : u2tzjtne@gmail.com
 *     time   : 2018/10/08
 *     desc   :公交二维码(禁止截图)
 *     version: 1.0
 * </pre>
 */
public class JsApi extends UZModule {

    private UZWebView mUZWebView;

    public JsApi(UZWebView webView) {
        super(webView);
        this.mUZWebView = webView;
    }

    //公交二维码页面
    public void jsmethod_startNativeBusQRCodeActivity(final UZModuleContext moduleContext) {
//        String type = moduleContext.optString("type");
//        String token = moduleContext.optString("token");
//        String userId = moduleContext.optString("userId");
//        String coordinate = moduleContext.optString("coordinate");
//        String deviceId = moduleContext.optString("deviceId");
//        String serverUrl = moduleContext.optString("serverUrl");

        String userPhoto = moduleContext.optString("userPhoto");
        String userName = moduleContext.optString("userName");

        String type = "2";
        String token = "fa8c812732a347705fff06c6046231dd";
        String userId = "613967";
        String coordinate = "118.781627,31.980357";
        String deviceId = "863076031756691";
        String serverUrl = "https://xztsbank.com.cn/manyibaoServer/";


        if (!TextUtils.isEmpty(type)
                && !TextUtils.isEmpty(token)
                && !TextUtils.isEmpty(userId)
                && !TextUtils.isEmpty(coordinate)
                && !TextUtils.isEmpty(deviceId)
                && !TextUtils.isEmpty(serverUrl)) {
            if (!serverUrl.endsWith("/")) serverUrl = serverUrl + "/";
            if (!serverUrl.startsWith("http")) serverUrl = "http://" + serverUrl;
            Intent intent = new Intent();
            intent.putExtra("token", token);
            intent.putExtra("userId", userId);
            intent.putExtra("coordinate", coordinate);
            intent.putExtra("deviceId", deviceId);
            intent.putExtra("serverUrl", serverUrl);
            switch (type) {
                case "1":
                    intent.putExtra("userPhoto", userPhoto);
                    intent.putExtra("userName", userName);
                    intent.setClass(context(), NativeBusQRCodeActivity.class);
                    break;
                case "2":
                    intent.setClass(context(), NativeBusQRCodeActivity2.class);
                    EventBus.getDefault().postSticky(moduleContext);
                    if (mUZWebView != null) EventBus.getDefault().postSticky(mUZWebView);
                    break;
            }
            startActivity(intent);
        } else {
            Toast.makeText(context(), context().getResources().getString(R.string.bus_missing_parameters), Toast.LENGTH_SHORT).show();
        }
    }
}
