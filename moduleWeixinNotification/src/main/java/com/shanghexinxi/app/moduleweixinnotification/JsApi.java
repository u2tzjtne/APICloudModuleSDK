package com.shanghexinxi.app.moduleweixinnotification;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.uzmap.pkg.uzcore.UZWebView;
import com.uzmap.pkg.uzcore.uzmodule.UZModule;
import com.uzmap.pkg.uzcore.uzmodule.UZModuleContext;

import static android.content.Context.NOTIFICATION_SERVICE;


public class JsApi extends UZModule {

    private int requestCode = (int) SystemClock.uptimeMillis();

    public JsApi(UZWebView webView) {
        super(webView);
        initWindowManager();
    }

    //仿微信通知
    public void jsmethod_showNotification(final UZModuleContext moduleContext) {
        //notify_headUp();

        //createFloatView("感谢各位老铁!");
        showNotifi();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void showNotification() {
        // 获取Service
        final WindowManager mWindowManager = (WindowManager) activity().getSystemService(Context.WINDOW_SERVICE);
        LayoutInflater layoutInflater = activity().getLayoutInflater();
        final View view = layoutInflater.inflate(R.layout.notification_item, null);
        final ObjectAnimator o = ObjectAnimator.ofFloat(view, "translationY", -150, 0f);
        o.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                final ObjectAnimator o2 = ObjectAnimator.ofFloat(view, "translationY", 0f, -150f);
                o2.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        if (mWindowManager != null) {
                            mWindowManager.removeView(view);
                        }
                        o2.cancel();
                    }
                });

                o2.setDuration(1000).setStartDelay(1500);//执行一秒 ， 延迟1.5秒
                o2.start();
                o.cancel();

            }
        });
        o.setDuration(1000).start();

        // 设置窗口类型，一共有三种Application windows, Sub-windows, System windows
        WindowManager.LayoutParams mWindowParams = new WindowManager.LayoutParams();
        // API中以TYPE_开头的常量有23个
        mWindowParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY;
        mWindowParams.flags = WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        ;
        // 设置期望的bitmap格式
        mWindowParams.format = PixelFormat.TRANSLUCENT; //设置背景透明

        // 以下属性在Layout Params中常见重力、坐标，宽高
        mWindowParams.gravity = Gravity.LEFT | Gravity.TOP;

        mWindowParams.x = 0;
        mWindowParams.y = 0;

        mWindowParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        mWindowParams.height = 150;//WindowManager.LayoutParams. WRAP_CONTENT;

        // 添加指定视图
        if (mWindowManager != null) {
            mWindowManager.addView(view, mWindowParams);
        }
    }


    /**
     * Android 5.0 新特性：悬浮式通知
     */
    private void notify_headUp() {
        //设置想要展示的数据内容
        int smallIcon = R.drawable.hl_smallicon;
        int largeIcon = R.drawable.fbb_largeicon;
        String ticker = "您有一条新通知";
        String title = "范冰冰";
        String content = "文明，今晚在希尔顿酒店2016号房哈";
        Intent intent = new Intent(context(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context(),
                requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);


        int lefticon = R.drawable.hl_message;
        String lefttext = "回复";
        Intent leftIntent = new Intent();
        leftIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent leftPendingIntent = PendingIntent.getActivity(context(),
                requestCode, leftIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        int righticon = R.drawable.hl_call;
        String righttext = "拨打";
        Intent rightIntent = new Intent(context(), MainActivity.class);
        rightIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent rightPendingIntent = PendingIntent.getActivity(context(),
                requestCode, rightIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        //实例化工具类，并且调用接口
        NotifyUtil notify8 = new NotifyUtil(context(), 8);
        notify8.notify_HeadUp(pendingIntent, smallIcon, largeIcon, ticker, title, content, lefticon, lefttext, leftPendingIntent, righticon, righttext, rightPendingIntent, true, true, false);
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void showNotifi(){
        NotificationManager manager = (NotificationManager) context().getSystemService(NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(context());
        builder.setContentTitle("横幅通知");
        builder.setContentText("请在设置通知管理中开启消息横幅提醒权限");
        builder.setDefaults(Notification.DEFAULT_ALL);
        builder.setSmallIcon(R.drawable.fbb_largeicon);
        builder.setLargeIcon(BitmapFactory.decodeResource(context().getResources(), R.drawable.fbb_largeicon));
        Intent intent = new Intent(context(), MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(context(), 1, intent, 0);
        builder.setContentIntent(pIntent);
        //这句是重点
        builder.setFullScreenIntent(pIntent, true);
        builder.setAutoCancel(true);
        Notification notification = builder.build();
        manager.notify((int) System.currentTimeMillis(), notification);
    }

    private View view;
    private WindowManager wm;
    private boolean showWm = true;//默认是应该显示悬浮通知栏
    private WindowManager.LayoutParams params;

    private void initWindowManager() {
        wm = (WindowManager) context().getApplicationContext().getSystemService(
                Context.WINDOW_SERVICE);
        params = new WindowManager.LayoutParams();
        //注意是TYPE_SYSTEM_ERROR而不是TYPE_SYSTEM_ALERT
        //前面有SYSTEM才可以遮挡状态栏，不然的话只能在状态栏下显示通知栏
        params.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
        params.format = PixelFormat.TRANSPARENT;
        //设置必须触摸通知栏才可以关掉
        params.flags = WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                | WindowManager.LayoutParams.FLAG_FULLSCREEN
                | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;

        // 设置通知栏的长和宽
        params.width = wm.getDefaultDisplay().getWidth();
        params.height = 200;
        params.gravity = Gravity.TOP;
    }


    private void createFloatView(String str) {

        view = LayoutInflater.from(context()).inflate(R.layout.notification_item, null);
        //在这里你可以解析你的自定义的布局成一个View
//        if (showWm) {
//            wm.addView(view, params);
//            showWm = false;
//        } else {
//            wm.updateViewLayout(view, params);
//        }
        wm.addView(view, params);

        view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        wm.removeViewImmediate(view);
                        view = null;
                        break;
                    case MotionEvent.ACTION_MOVE:

                        break;
                }
                return true;
            }
        });

    }


    @Override
    protected void onClean() {
        super.onClean();

    }
}
