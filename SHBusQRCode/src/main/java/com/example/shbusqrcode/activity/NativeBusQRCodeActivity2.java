package com.example.shbusqrcode.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.shbusqrcode.R;
import com.example.shbusqrcode.api.ServerApi;
import com.example.shbusqrcode.autosize.utils.AutoSizeUtils;
import com.example.shbusqrcode.util.NetworkUtil;
import com.example.shbusqrcode.util.QRCodeUtil;
import com.example.shbusqrcode.view.dialog.KProgressHUD;
import com.uzmap.pkg.uzcore.UZWebView;
import com.uzmap.pkg.uzcore.uzmodule.UZModuleContext;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NativeBusQRCodeActivity2 extends Activity {
    private ImageView mQRCode;
    private String userId, deviceId, token, coordinate, recSign, errorCode, serverUrl;
    private KProgressHUD dialog;
    private UZModuleContext moduleContext;
    private UZWebView mWebView;
    private static final long TOTAL_TIME = 60000;
    private static final long ONECE_TIME = 30000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_native_bus_qrcode2);
        mQRCode = findViewById(R.id.image_qr_code);
        dialog = KProgressHUD.create(this).setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        setWindowMaxBrightness();//调整屏幕亮度
        Intent intent = getIntent();
        if (intent != null) {
            userId = intent.getStringExtra("userId");
            deviceId = intent.getStringExtra("deviceId");
            token = intent.getStringExtra("token");
            coordinate = intent.getStringExtra("coordinate");
            serverUrl = intent.getStringExtra("serverUrl");
            createQRCode();
        }
    }

    //自动刷新
    private CountDownTimer countDownTimer = new CountDownTimer(TOTAL_TIME, ONECE_TIME) {
        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {
            createQRCode();
        }
    };

    //点击刷新
    public void refreshClick(View v) {
        countDownTimer.cancel();
        createQRCode();
    }

    //返回按钮
    public void backClick(View v) {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        countDownTimer.cancel();
        if (moduleContext != null) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("action", "back");
                if (moduleContext != null) moduleContext.success(jsonObject, true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (mWebView!=null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                mWebView.evaluateJavascript("javascript:callJS()", new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String value) {
                        Log.d("JS回调的内容:",value);
                    }
                });
            }
        }
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                //finish();
//            }
//        }, 200);
    }

    //扫一扫
    public void scanClick(View v) {
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        countDownTimer.cancel();
        if (moduleContext != null) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("action", "scan");
                if (moduleContext != null) moduleContext.success(jsonObject, true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        finish();
    }

    //请求数据并生成二维码
    private void createQRCode() {
        if (NetworkUtil.isNetworkAvailable(NativeBusQRCodeActivity2.this)) {
            if (!dialog.isShowing()) dialog.show();
            try {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(serverUrl)
                        .client(new OkHttpClient())
                        .build();
                ServerApi serverApi = retrofit.create(ServerApi.class);
                String userAgent = new WebView(this).getSettings().getUserAgentString();
                Call<ResponseBody> call = serverApi.getQrCodeData(userAgent, token, deviceId, userId, coordinate, deviceId);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                        if (response.code() == 200) {
                            try {
                                String json = response.body().string();
                                if (json != null) {
                                    JSONObject jsonObject = new JSONObject(json);
                                    JSONObject body = jsonObject.getJSONObject("body");
                                    errorCode = body.getString("errorCode");
                                    recSign = body.getString("recSign");
                                    createQRAndDisplay();
                                } else {
                                    showErrorMessage(R.string.bus_error_message_qrcode, true);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                                showErrorMessage(R.string.bus_error_message_qrcode, true);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                showErrorMessage(R.string.bus_error_message_qrcode, true);
                            }
                        } else {
                            showErrorMessage(R.string.bus_error_message_qrcode, true);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                        showErrorMessage(R.string.bus_error_message_qrcode, true);
                    }
                });
            } catch (Exception e) {
                Toast.makeText(NativeBusQRCodeActivity2.this, "发送请求失败:" + e.getMessage(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

        } else {
            createQRAndDisplay();
        }
    }

    //生成并显示二维码
    private void createQRAndDisplay() {
        if (!TextUtils.isEmpty(errorCode) && !TextUtils.isEmpty(recSign) && errorCode.equals("000000")) {
            if (!dialog.isShowing()) dialog.show();
            String str = createQRStr(recSign);
            final Bitmap bitmap = QRCodeUtil.createQRCode(str, AutoSizeUtils.dp2px(NativeBusQRCodeActivity2.this, 173));
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (bitmap != null) {
                        mQRCode.setImageBitmap(bitmap);
                    } else {
                        showErrorMessage(R.string.bus_error_message_qrcode, false);
                    }
                    countDownTimer.cancel();
                    countDownTimer.start();
                }
            });
        }
        if (dialog.isShowing()) dialog.dismiss();
    }

    //生成二维码字符串
    private String createQRStr(String recSign) {
        if (!TextUtils.isEmpty(recSign)) {
            //当前时间
            String currentDate = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            //随机数
            int random = (int) (Math.random() * 10000 % 9 + 1);
            //当前时间乘以随机数
            long date = Long.parseLong(currentDate);
            StringBuilder dates = new StringBuilder(String.valueOf(date * random));
            //补足15位
            while (dates.length() < 15) {
                dates.insert(0, "0");
            }
            String str1 = random + "" + dates.toString() + recSign;
            StringBuilder lengths = new StringBuilder(String.valueOf(str1.length()));
            while (lengths.length() < 4) {
                lengths.insert(0, "0");
            }
            return ':' + lengths.toString() + random + "" + dates.toString() + recSign;
        }
        return null;
    }

    //设置当前窗口最高亮度
    private void setWindowMaxBrightness() {
        Window window = getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.screenBrightness = 1;
        window.setAttributes(lp);
    }

    //显示异常信息
    private void showErrorMessage(final int message, final boolean isReTry) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (dialog.isShowing())
                    dialog.dismiss();
                Toast.makeText(NativeBusQRCodeActivity2.this, getResources().getString(message), Toast.LENGTH_SHORT).show();
                if (isReTry) {
                    countDownTimer.cancel();
                    countDownTimer.start();
                }
            }
        });
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(UZModuleContext moduleContext) {
        if (moduleContext != null) this.moduleContext = moduleContext;
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(UZWebView webView) {
        if (webView != null) this.mWebView = webView;
    }

    @Override
    public void onStart() {
        super.onStart();
        // 注册订阅事件
        if (null != EventBus.getDefault()) EventBus.getDefault().unregister(this);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (dialog.isShowing()) {
            dialog.dismiss();
        }
        countDownTimer.cancel();
        if (moduleContext != null) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("action", "back");
                if (moduleContext != null) moduleContext.success(jsonObject, true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 200);
    }
}
