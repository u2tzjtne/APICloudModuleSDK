package com.shanghe.modulenavidemo;

import android.content.Intent;

import com.shanghe.modulenavidemo.activity.NaviActivity;
import com.uzmap.pkg.uzcore.UZWebView;
import com.uzmap.pkg.uzcore.uzmodule.UZModule;
import com.uzmap.pkg.uzcore.uzmodule.UZModuleContext;

public class JsApi extends UZModule {
    public JsApi(UZWebView webView) {
        super(webView);
    }

    //语音合成
    public void jsmethod_SHNavi(final UZModuleContext moduleContext) {
        double startLat = moduleContext.optDouble("startLat");
        double startLng = moduleContext.optDouble("startLng");
        double endLat = moduleContext.optDouble("endLat");
        double endLng = moduleContext.optDouble("endLng");
        int naviType = moduleContext.optInt("naviType");

        Intent intent = new Intent(context(), NaviActivity.class);
        intent.putExtra("startLat", startLat);
        intent.putExtra("startLng", startLng);
        intent.putExtra("endLat", endLat);
        intent.putExtra("endLng", endLng);
        intent.putExtra("naviType", naviType);

        startActivity(intent);
    }
}
