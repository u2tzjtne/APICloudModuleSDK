package com.shanghe.shtts;

import android.media.AudioManager;
import android.text.TextUtils;

import com.baidu.tts.auth.AuthInfo;
import com.baidu.tts.client.SpeechSynthesizer;
import com.baidu.tts.client.TtsMode;
import com.shanghe.shtts.util.FileUtil;
import com.uzmap.pkg.uzcore.UZWebView;
import com.uzmap.pkg.uzcore.uzmodule.UZModule;
import com.uzmap.pkg.uzcore.uzmodule.UZModuleContext;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class JsApi extends UZModule {

    //初始化失败
    private final int ERROR_CODE_INIT = 1001;
    //播放失败
    private final int ERROR_CODE_PLAY = 1002;

    // TtsMode.MIX; 离在线融合，在线优先； TtsMode.ONLINE 纯在线； 没有纯离线
    private TtsMode ttsMode = TtsMode.MIX;

    // 文本模型文件
    private final String TEXT_FILENAME = FileUtil.getAssetsCacheFile(context(), "bd_etts_text.dat");

    // 声学模型文件 ，f7是离线女声
    private final String MODEL_FILENAME = FileUtil.getAssetsCacheFile(context(), "bd_etts_common_speech_f7_mand_eng_high_am-mix_v3.0.0_20170512.dat");


    private SpeechSynthesizer mSpeechSynthesizer;

    private UZModuleContext uzModuleContext;

    private int result;


    public JsApi(UZWebView webView) {
        super(webView);
        String appId = getSecureValue("baiDuTTS_appID_android");
        String appKey = getSecureValue("baiDuTTS_apiKey_android");
        String secretKey = getSecureValue("baiDuTTS_secretKey_android");

//      String appId = "11710684";
//      String appKey = "deVeMTL1VtCPWXGwD4KYBQGz";
//      String secretKey = "5bBxoitqMgAZgUaBRN3VetDBiys63ejZ";
//
        if (!TextUtils.isEmpty(appId) && !TextUtils.isEmpty(appKey) && !TextUtils.isEmpty(secretKey)) {
            initTTs(appId, appKey, secretKey);
        }
    }

    //语音合成
    public void jsmethod_SHSpeak(final UZModuleContext moduleContext) {
        this.uzModuleContext = moduleContext;
        String text = moduleContext.optString("text");
        if (!TextUtils.isEmpty(text)) {
            speak(text);
        }
    }

    @Override
    protected void onClean() {
        super.onClean();
        if (mSpeechSynthesizer != null) {
            mSpeechSynthesizer.stop();
            mSpeechSynthesizer.release();
            mSpeechSynthesizer = null;
        }
    }

    private void speak(String text) {
        /* 以下参数每次合成时都可以修改
         *  mSpeechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEAKER, "0");
         *  设置在线发声音人： 0 普通女声（默认） 1 普通男声 2 特别男声 3 情感男声<度逍遥> 4 情感儿童声<度丫丫>
         *  mSpeechSynthesizer.setParam(SpeechSynthesizer.PARAM_VOLUME, "5"); 设置合成的音量，0-9 ，默认 5
         *  mSpeechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEED, "5"); 设置合成的语速，0-9 ，默认 5
         *  mSpeechSynthesizer.setParam(SpeechSynthesizer.PARAM_PITCH, "5"); 设置合成的语调，0-9 ，默认 5
         *
         *  mSpeechSynthesizer.setParam(SpeechSynthesizer.PARAM_MIX_MODE, SpeechSynthesizer.MIX_MODE_DEFAULT);
         *  MIX_MODE_DEFAULT 默认 ，wifi状态下使用在线，非wifi离线。在线状态下，请求超时6s自动转离线
         *  MIX_MODE_HIGH_SPEED_SYNTHESIZE_WIFI wifi状态下使用在线，非wifi离线。在线状态下， 请求超时1.2s自动转离线
         *  MIX_MODE_HIGH_SPEED_NETWORK ， 3G 4G wifi状态下使用在线，其它状态离线。在线状态下，请求超时1.2s自动转离线
         *  MIX_MODE_HIGH_SPEED_SYNTHESIZE, 2G 3G 4G wifi状态下使用在线，其它状态离线。在线状态下，请求超时1.2s自动转离线
         */
        if (mSpeechSynthesizer == null) {
            return;
        }
        result = mSpeechSynthesizer.speak(text);
        checkResult(ERROR_CODE_PLAY);
    }

    /**
     * 注意此处为了说明流程，故意在UI线程中调用。
     * 实际集成中，该方法一定在新线程中调用，并且该线程不能结束。具体可以参考NonBlockSyntherizer的写法
     *
     * @param appId
     * @param appKey
     * @param secretKey
     */
    private void initTTs(String appId, String appKey, String secretKey) {
        boolean isMix = ttsMode.equals(TtsMode.MIX);
        boolean isSuccess;
        if (isMix) {
            // 检查2个离线资源是否可读
            isSuccess = checkOfflineResources();
            if (!isSuccess) {
                return;
            }
        }
        // 1. 获取实例
        mSpeechSynthesizer = SpeechSynthesizer.getInstance();
        mSpeechSynthesizer.setContext(context());

        // 3. 设置appId，appKey.secretKey
        result = mSpeechSynthesizer.setAppId(appId);
        checkResult(ERROR_CODE_INIT);
        result = mSpeechSynthesizer.setApiKey(appKey, secretKey);
        checkResult(ERROR_CODE_INIT);
        // 4. 支持离线的话，需要设置离线模型
        if (isMix) {
            // 检查离线授权文件是否下载成功，离线授权文件联网时SDK自动下载管理，有效期3年，3年后的最后一个月自动更新。
            isSuccess = checkAuth();
            if (!isSuccess) {
                return;
            }
            // 文本模型文件路径 (离线引擎使用)， 注意TEXT_FILENAME必须存在并且可读
            result = mSpeechSynthesizer.setParam(SpeechSynthesizer.PARAM_TTS_TEXT_MODEL_FILE, TEXT_FILENAME);
            checkResult(ERROR_CODE_INIT);
            // 声学模型文件路径 (离线引擎使用)， 注意TEXT_FILENAME必须存在并且可读
            result = mSpeechSynthesizer.setParam(SpeechSynthesizer.PARAM_TTS_SPEECH_MODEL_FILE, MODEL_FILENAME);
            checkResult(ERROR_CODE_INIT);
        }

        // 5. 以下setParam 参数选填。不填写则默认值生效
        // 设置在线发声音人： 0 普通女声（默认） 1 普通男声 2 特别男声 3 情感男声<度逍遥> 4 情感儿童声<度丫丫>
        result = mSpeechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEAKER, "0");
        checkResult(ERROR_CODE_INIT);
        // 设置合成的音量，0-9 ，默认 5
        result = mSpeechSynthesizer.setParam(SpeechSynthesizer.PARAM_VOLUME, "9");
        checkResult(ERROR_CODE_INIT);
        // 设置合成的语速，0-9 ，默认 5
        result = mSpeechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEED, "5");
        checkResult(ERROR_CODE_INIT);
        // 设置合成的语调，0-9 ，默认 5
        result = mSpeechSynthesizer.setParam(SpeechSynthesizer.PARAM_PITCH, "5");
        checkResult(ERROR_CODE_INIT);


        // 该参数设置为TtsMode.MIX生效。即纯在线模式不生效。
        // MIX_MODE_DEFAULT 默认 ，wifi状态下使用在线，非wifi离线。在线状态下，请求超时6s自动转离线
        // MIX_MODE_HIGH_SPEED_SYNTHESIZE_WIFI wifi状态下使用在线，非wifi离线。在线状态下， 请求超时1.2s自动转离线
        // MIX_MODE_HIGH_SPEED_NETWORK ， 3G 4G wifi状态下使用在线，其它状态离线。在线状态下，请求超时1.2s自动转离线
        // MIX_MODE_HIGH_SPEED_SYNTHESIZE, 2G 3G 4G wifi状态下使用在线，其它状态离线。在线状态下，请求超时1.2s自动转离线
        result = mSpeechSynthesizer.setParam(SpeechSynthesizer.PARAM_MIX_MODE, SpeechSynthesizer.MIX_MODE_HIGH_SPEED_SYNTHESIZE_WIFI);
        checkResult(ERROR_CODE_INIT);
        result = mSpeechSynthesizer.setAudioStreamType(AudioManager.MODE_IN_CALL);
        checkResult(ERROR_CODE_INIT);
        // 6. 初始化
        result = mSpeechSynthesizer.initTts(ttsMode);
        checkResult(ERROR_CODE_INIT);
    }

    private void checkResult(int errorCode) {
        if (result != 0) {
            try {
                JSONObject jsonObject = new JSONObject();
                switch (errorCode) {
                    case ERROR_CODE_INIT:
                        jsonObject.put("code", "1");
                        jsonObject.put("result", "加载离线文件出错");
                        break;
                    case ERROR_CODE_PLAY:
                        jsonObject.put("code", "2");
                        jsonObject.put("result", "播放语音出错");
                        break;
                }
                uzModuleContext.success(jsonObject, true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 检查appId ak sk 是否填写正确，另外检查官网应用内设置的包名是否与运行时的包名一致。本demo的包名定义在build.gradle文件中
     *
     * @return
     */
    private boolean checkAuth() {
        AuthInfo authInfo = mSpeechSynthesizer.auth(ttsMode);
        return authInfo.isSuccess();
    }


    /**
     * 检查 TEXT_FILENAME, MODEL_FILENAME 这2个文件是否存在，不存在请自行从assets目录里手动复制
     *
     * @return
     */
    private boolean checkOfflineResources() {
        String[] filenames = {TEXT_FILENAME, MODEL_FILENAME};
        for (String path : filenames) {
            File f = new File(path);
            if (!f.canRead()) {
                return false;
            }
        }
        return true;
    }
}
